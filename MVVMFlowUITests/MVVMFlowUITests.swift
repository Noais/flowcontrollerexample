//
//  MVVMFlowUITests.swift
//  MVVMFlowUITests
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import XCTest
@testable import MVVMFlow

class MVVMFlowUITests: XCTestCase {
        
    let app = XCUIApplication()
    override func setUp() {
        super.setUp()
        continueAfterFailure = true
        app.launchArguments.append("uitest-list")
        app.launch()
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testListViewControllerItens() {
        XCTAssertTrue(app.tables.cells.count == 21)
    }
    
    func testListViewControllerOpenDetail() {
        app.tables.cells.element(boundBy: 0).tap()
    }
    
}
