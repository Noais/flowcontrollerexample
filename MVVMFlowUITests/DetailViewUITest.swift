//
//  DetailViewUITest.swift
//  MVVMFlow
//
//  Created by David Ferreira on 16/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import XCTest
@testable import MVVMFlow

class DetailViewUITest: XCTestCase {
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = true
        app.launchArguments.append("uitest-detail")
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDetectLabels() {
        XCTAssertTrue(app.staticTexts["Name"].exists)
        XCTAssertTrue(app.staticTexts["Description"].exists)
    }
    
    func testErrorLabels() {
        XCTAssertFalse(app.staticTexts["NameFail"].exists)
        XCTAssertFalse(app.staticTexts["DescriptionFail"].exists)
    }
}
