//
//  TableViewUITest.swift
//  MVVMFlow
//
//  Created by David Ferreira on 16/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import XCTest
@testable import MVVMFlow

class TableViewUITest: XCTestCase {
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = true
        app.launchArguments.append("uitest-list")
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testListViewControllerItens() {
        // This test is dependent on the test device being used! 7+ is 18, 6S is 10...
        XCTAssertTrue(app.tables.element.cells.element(boundBy: 4).exists)
    }
}
