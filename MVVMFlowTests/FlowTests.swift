//
//  FlowTests.swift
//  MVVMFlow
//
//  Created by David Ferreira on 16/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import XCTest
@testable import MVVMFlow

class FlowTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMainFlowWindow() {
        let window = UIWindow(frame : UIScreen.main.bounds)
        let configure = FlowConfigure(window: window, navigationController: nil, parent: nil)
        let mainFlow = MainFlowController(configure: configure)
        mainFlow.start()
        XCTAssertTrue(true)
    }
    
    func testMainFlowNavigation() {
        let navigationController = UINavigationController()
        let configure = FlowConfigure(window: nil, navigationController: navigationController, parent: nil)
        let mainFlow = MainFlowController(configure: configure)
        mainFlow.start()
        XCTAssertTrue(true)
    }
    
    func testProductsFlowWindow() {
        let window = UIWindow(frame : UIScreen.main.bounds)
        let productConf = FlowConfigure(window: window, navigationController: nil, parent: nil)
        let childFlow = ProductsFlowController(configure: productConf)
        childFlow.start()
        XCTAssertTrue(true)
        childFlow.openDetailFor(id: 0)
        XCTAssertTrue(true)
    }
    
    func testProductsFlowNavigation() {
        let navigationController = UINavigationController()
        let productConf = FlowConfigure(window: nil, navigationController: navigationController, parent: nil)
        let childFlow = ProductsFlowController(configure: productConf)
        childFlow.start()
        XCTAssertTrue(true)
        childFlow.openDetailFor(id: 0)
        XCTAssertTrue(true)
    }
    
    func testDetailProductFlowWindow() {
        let window = UIWindow(frame : UIScreen.main.bounds)
        let detail = FlowConfigure(window: window, navigationController: nil, parent: nil)
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        if let item = list.item(ofIndex: 0) {
            let childFlow = ProductDetailFlowController(configure: detail,item: item)
            childFlow.start()
            XCTAssertTrue(true)
        }
        let childFlow = ProductDetailFlowController(configure: detail)
        childFlow.close()
        XCTAssertTrue(true)
    }
    
    func testDetailProductFlowNavigation() {
        let navigationController = UINavigationController()
        let detail = FlowConfigure(window: nil, navigationController: navigationController, parent: nil)
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        if let item = list.item(ofIndex: 0) {
            let childFlow = ProductDetailFlowController(configure: detail,item: item)
            childFlow.start()
            XCTAssertTrue(true)
        }
        let childFlow = ProductDetailFlowController(configure: detail)
        childFlow.close()
        XCTAssertTrue(true)
    }
    
    func testFlowDetectTypeMain() {
        let window = UIWindow(frame : UIScreen.main.bounds)
        let configure = FlowConfigure(window: window, navigationController: nil, parent: nil)
        XCTAssertTrue(configure.whichFlowAmI() == .main)
    }
    
    func testFlowDetectTypeNavigation() {
        let navigation = UINavigationController()
        let configure = FlowConfigure(window: nil, navigationController: navigation, parent: nil)
        XCTAssertTrue(configure.whichFlowAmI() == .navigation)
    }
    
    func testFlowDetectTypeNil() {
        let configure = FlowConfigure(window: nil, navigationController: nil, parent: nil)
        XCTAssertNil(configure.whichFlowAmI())
    }
}
