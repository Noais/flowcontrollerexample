//
//  MVVMFlowTests.swift
//  MVVMFlowTests
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import XCTest
@testable import MVVMFlow

class MVVMFlowTests: XCTestCase {    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFailCount() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        XCTAssertNotEqual(list.count(), 0)
    }
    
    func testSuccessCount() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        XCTAssertEqual(list.count(), 21)
    }
    
    func testFailGetItem() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        XCTAssertNil(list.item(ofIndex: -1))
        XCTAssertNil(list.item(ofIndex: list.count()))
    }
    
    func testSuccessGetItem() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        XCTAssertNotNil(list.item(ofIndex: 0))
        XCTAssertNotNil(list.item(ofIndex: list.count() - 1))
    }
    
    func testSuccessDetailItem() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let list = ListViewModel<ProductModel>(model: modelProduct)
        for i in 0 ..< list.count() {
            let item = list.item(ofIndex: i)
            XCTAssertNotNil(item?.title())
            XCTAssertNotNil(item?.text())
        }
    }    
}
