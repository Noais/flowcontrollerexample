//
//  ViewControllersTests.swift
//  MVVMFlow
//
//  Created by David Ferreira on 16/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import XCTest
@testable import MVVMFlow

class ViewControllersTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testListViewController() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let viewModel = ListViewModel<ProductModel>(model: modelProduct)
        let configureTable = ConfigureTable(styleTable: .plain, title: "List of Products",delegate: DelegateMock())
        _ = ListTableViewController<ProductModel>(viewModel: viewModel, configure: configureTable) { product, cell in
            XCTAssertNotNil(product)
            XCTAssertNotNil(cell)
        }
    }
    

    func testDetailController() {
        let clockService = ClockService()
        let modelProduct = ProductModel(s: clockService)
        let viewModel = ListViewModel<ProductModel>(model: modelProduct)
        let item = viewModel.item(ofIndex: 0)
        let configureDetail = ConfigureDetail(title: "Detail", delegate: DetailDelegateMock(), modal: false)
        let _ = ProductDetailViewController<Product>(viewModel: item!, configure: configureDetail)
        XCTAssertTrue(true)
    }
    
    
    class DelegateMock: ListTableViewControllerDelegate {
        func openDetailFor(id : Int) {
            // Do nothing for the tests
        }
    }
    
    class DetailDelegateMock: DetailViewControllerDelegate {
        func close() {
            // Do nothing for the tests
        }
        
        func goToDedicatedView(){
            // Do nothing for the tests
        }
    }
}
