//
//  ProductDetailViewController.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit


class ProductDetailFlowController: FlowController, DetailViewControllerDelegate {
    
    fileprivate let configure: FlowConfigure
    fileprivate var viewModel: DetailModel?
    
    
    init(configure : FlowConfigure, item : DetailModel) {
        viewModel = item
        self.configure = configure
    }
    
    required init(configure : FlowConfigure) {
        self.configure = configure
    }
    
    func start() {
        let configureDetail = ConfigureDetail(title: "Detail", delegate: self, modal: true)
        let viewController = ProductDetailViewController<Product>(viewModel: viewModel!, configure: configureDetail)
        
        viewController.modalPresentationStyle = .overCurrentContext
        configure.navigationController?.present(viewController, animated: false,completion : nil)
    }
    
    func close() {
        // Go back to the list
    }

    
    func goToDedicatedView() {
            let dedicated = FlowConfigure(window: nil, navigationController: configure.navigationController, parent: self)
            let childFlow = DedicatedFlowController(configure: dedicated)
            childFlow.start()
    }
}
