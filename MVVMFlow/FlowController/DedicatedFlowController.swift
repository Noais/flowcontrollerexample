//
//  DedicatedViewFlowController.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import Foundation

protocol DedicatedViewControllerDelegate {
    func close()
}


class DedicatedFlowController: FlowController, DedicatedViewControllerDelegate {
    
    fileprivate let configure: FlowConfigure
    
    required init(configure : FlowConfigure) {
        self.configure = configure
    }
    
    func start() {
        let configureDedicated = ConfigureDedicated(delegate: self)
        let viewController = DedicatedViewController(configure: configureDedicated)
        
        //viewController.modalPresentationStyle = .overCurrentContext
        configure.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func close() {
        // Go back to the list
    }
}
