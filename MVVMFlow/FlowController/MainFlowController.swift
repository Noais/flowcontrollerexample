//
//  ViewController.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit

class MainFlowController: FlowController {

    let configure: FlowConfigure
    var childFlow: FlowController?
    
    required init(configure : FlowConfigure) {
        self.configure = configure
    }
    
    func start() {
        let navigationController = UINavigationController()
        if let frame = configure.window?.bounds {
            navigationController.view.frame = frame
        }
        
        configure.window?.rootViewController = navigationController
        configure.window?.makeKeyAndVisible()
        
        let prodConf = FlowConfigure(window: nil, navigationController: navigationController, parent: self)
        childFlow = ProductsFlowController(configure: prodConf)
        childFlow?.start()
    }
}

