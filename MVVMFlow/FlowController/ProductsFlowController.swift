//
//  ProductsFlowController.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit

class ProductsFlowController: FlowController, ListTableViewControllerDelegate {
    
    private let configure: FlowConfigure
    private let viewModel: ListViewModel<ProductModel>
    
    required init(configure: FlowConfigure) {
        self.configure = configure
        let clockService = ClockService()
        let products = ProductModel(s: clockService)
        viewModel = ListViewModel<ProductModel>(model: products)
    }
    
    func start() {
        let configureTable = ConfigureTable(styleTable: .plain, title: "List of Products",delegate: self)
        let viewController = ListTableViewController<ProductModel>(viewModel: viewModel, configure: configureTable) { product, cell in
            cell.textLabel?.text = product.name
        }
    
        configure.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func openDetailFor(id : Int) {
        if let item = viewModel.item(ofIndex: id) {
            let detail = FlowConfigure(window: nil, navigationController: configure.navigationController, parent: self)
            let childFlow = ProductDetailFlowController(configure: detail,item: item)
            childFlow.start()
        }
    }
}
