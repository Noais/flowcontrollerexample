//
//  ListViewModel.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import Foundation

open class ListViewModel<T : ListModel> {
    let model : T
    
    init(model : T) {
        self.model = model
    }
    
    func count() -> Int {
        return model.all().count
    }
    
    func item(ofIndex index: Int) -> T.Model? {
        return model.one(at : index)
    }
}
