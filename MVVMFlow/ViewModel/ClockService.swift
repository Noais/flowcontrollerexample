//
//  ClockService.swift
//  MVVMFlow
//
//  Created by David Ferreira on 16/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import Foundation

func loadBase() -> [Product] {
    return [
        Product(name : "ambassador", description: "Ambassador is ..."),
        Product(name: "appdir", description: "App dir is ..."),
        Product(name: "argentina", description: "Argentina is ..."),
        Product(name: "astronaut", description: "Astronaut is ..."),
        Product(name: "australiaFemale", description: "Australia Female is ..."),
        Product(name: "australiaMale", description: "Australia Male is ..."),
        Product(name: "aviatorFemale", description: "Aviator Female is ..."),
        Product(name: "biker", description: "Biker is ..."),
        Product(name: "binocularsNew", description: "Binoculars New is ..."),
        Product(name: "binoculars", description: "Binoculars is ..."),
        Product(name: "Black Suit", description: "Black Suit is ..."),
        Product(name: "Body Guard", description: "Body Guard is ..."),
        Product(name: "Campus Ambassador", description: "Campus Ambassador is ..."),
        Product(name: "Foodie Girl", description: "Foodie Girl is ..."),
        Product(name: "French", description: "French is ..."),
        Product(name: "Graduate", description: "Graduate is ..."),
        Product(name: "Lumberjack", description: "Lumberjack is ..."),
        Product(name: "Graduate", description: "Graduate is ..."),
        Product(name: "Mother", description: "Mother is ..."),
        Product(name: "Photo", description: "Photo is ..."),
        Product(name: "Sherlock UK", description: "Sherlock UK is ..."),
    ]
}



class ClockService: Service
{
    func fetchProducts() -> ([Product]?, ErrorType?) {
        
        return (loadBase(), nil)
    }
}
