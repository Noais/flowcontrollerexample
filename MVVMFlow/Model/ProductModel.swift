//
//  ProductModel.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit

protocol Service
{
    func fetchProducts() -> ([Product]?, ErrorType?)
}


struct Product: DetailModel {
    let name: String
    let description: String
    
    
    func title() -> String {
        return name
    }
  
    func text() -> String {
        return description
    }
}

class ProductModel: ListModel {
    typealias Model = Product
    private var service: Service
    
    init(s: Service)
    {
        service = s
    }
    
    
    func all() -> [Product] {
        print(self.initialPlayers()[0].name)
        return self.initialPlayers()
    }
    
    func one(at index: Int)  -> Product? {
        if index >= 0 && index < self.initialPlayers().count {
            return self.initialPlayers()[index]
        }
        return nil
    }
    
    func initialPlayers() -> [Product] {
        
        let result = self.service.fetchProducts() as ([Product]?, ErrorType?)
        
        if result.1 == nil{
            return result.0!
        }
        
        return []
    }
}
