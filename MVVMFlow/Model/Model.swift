//
//  Model.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import Foundation
import UIKit

public protocol Model {
    
}

public protocol ListModel : Model {
    associatedtype Model = Any
    func all() -> [Model]
    func one(at index: Int) -> Model?
}

protocol DetailModel : Model {
    func title() -> String
    func text() -> String
}
