//
//  DedicatedViewController.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit

struct ConfigureDedicated {
    let delegate: DedicatedViewControllerDelegate
}

class DedicatedViewController: UIViewController {
    var viewCenter: UIView?
    let configure: ConfigureDedicated
    
    init(configure: ConfigureDedicated) {
        self.configure = configure
        super.init(nibName: "DedicatedViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

