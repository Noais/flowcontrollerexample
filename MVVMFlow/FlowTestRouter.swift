//
//  FlowTestRouter.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit

class FlowTestRouter {
    
    static func selectRoute(_ window : UIWindow , routes : [String]) {
        if routes.contains("uitest-list") {
            let navigationController = UINavigationController()
            let frame = window.bounds
            navigationController.view.frame = frame
            
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            
            let prodConf = FlowConfigure(window: nil, navigationController: navigationController, parent: nil)
            let childFlow = ProductsFlowController(configure: prodConf)
            childFlow.start()
        } else if routes.contains("uitest-detail") {
            let navigationController = UINavigationController()
            let frame = window.bounds
            navigationController.view.frame = frame
            
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            
            let prodConf = FlowConfigure(window: nil, navigationController: navigationController, parent: nil)
            let modelMock = Product(name: "Name", description: "Description")
            let childFlow = ProductDetailFlowController(configure: prodConf, item: modelMock)
            childFlow.start()
        }else if routes.contains("uitest-dedicated") {
            let navigationController = UINavigationController()
            let frame = window.bounds
            navigationController.view.frame = frame
            
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            
            let prodConf = FlowConfigure(window: nil, navigationController: navigationController, parent: nil)
            let childFlow = DedicatedFlowController(configure: prodConf)
            childFlow.start()
        }
    }
}
