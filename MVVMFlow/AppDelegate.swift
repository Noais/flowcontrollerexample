//
//  AppDelegate.swift
//  MVVMFlow
//
//  Created by David Ferreira on 14/06/2017.
//  Copyright © 2017 David Ferreira. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame : UIScreen.main.bounds)
        
        if let window = window, ProcessInfo.processInfo.arguments.count > 1 {
            FlowTestRouter.selectRoute(window, routes: ProcessInfo.processInfo.arguments)
            return true
        }
        
        let configure = FlowConfigure(window: window, navigationController: nil, parent: nil)
        let mainFlow = MainFlowController(configure: configure)
        mainFlow.start()
        
        return true
    }
}

